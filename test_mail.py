#!/usr/bin/env python3

import smtplib,ssl
import getpass

sender_email = 'prasad.sawool@somaiya.edu'
receiver_email = 'thc.prasads@gmail.com'
password = getpass.getpass("enter password: \n")
port = 465

message = """ From: Prasad Sawool

Sub: Test
second mail
"""

context = ssl.create_default_context()
with smtplib.SMTP_SSL("smtp.gmail.com",port,context=context) as server:
    server.login(sender_email,password)
    server.sendmail(sender_email,receiver_email,message)
