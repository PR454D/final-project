#!/usr/bin/env python3
import os
from twilio.rest import Client

account_sid = os.environ['TWILIO_ACCOUNT_SID']
auth_token = os.environ['TWILIO_AUTH_TOKEN']

value_name = "Twilio"

client = Client(account_sid,auth_token)

if __name__ == '__main__':
    client.api.account.messages.create(
            to="+917498734024",
            from_="+17754067723",
            body=f"Hello from {value_name}!"
            )
