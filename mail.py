import os
import yagmail
import ssl

sender_email = 'prasad.sawool@somaiya.edu'
receiver_email = 'thc.prasads@gmail.com'

app_password = os.environ['APP_PASSWORD']


subject = "Test email"

content = ['Test mail sent']

if __name__ == '__main__':
    with yagmail.SMTP(sender_email, app_password) as yag:
        yag.send(receiver_email, subject, content)
        print("test email sent successfully")
